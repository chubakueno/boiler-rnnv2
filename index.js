import { Navigation } from "react-native-navigation";

import App1 from "./App1";
import App2 from "./App2";
import App3 from "./App3";

Navigation.registerComponent(`app1`, () => App1);
Navigation.registerComponent(`app2`, () => App2);
Navigation.registerComponent(`app3`, () => App3);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
      root: {
        sideMenu: {
          left: {
            component: {
              id: 'Sidebar',
              name: 'app1',
            }
          },
          center: {
            stack:{
              id: "AppRoot",
              options: {
                topBar: {
                  leftButtons: [
                    {
                      id: 'btnMenu',
                      icon: require('./assets/plus.png')
                    }
                  ],
                  rightButtons: [
                    {
                      id: 'btnOptions',
                      icon: require('./assets/three_dots_vertical.png'),
                    }
                  ],
                 
                },
              },
              children: [{
                component: {
                  id: "Home",
                  name: "app1"
                },
                bottomTabs: {
                    children: [{
                      component: {
                        name: 'app1',
                        passProps: {
                          text: 'This is tab 1'
                        },
                        options: {
                          bottomTab: {
                            text: 'Tab 1',
                            icon: require('./assets/plus.png'),
                            testID: 'FIRST_TAB_BAR_BUTTON'
                          }
                        }
                      }
                    },
                    {
                      component: {
                        name: 'app2',
                        passProps: {
                          text: 'This is tab 2'
                        },
                        options: {
                          bottomTab: {
                            text: 'Tab 2',
                            icon: require('./assets/plus.png'),
                            testID: 'SECOND_TAB_BAR_BUTTON'
                          }
                        }
                      }
                    },
                    {
                      component: {
                        name: 'app3',
                        passProps: {
                          text: 'This is tab 3'
                        },
                        options: {
                          bottomTab: {
                            text: 'Tab 3',
                            icon: require('./assets/plus.png'),
                            testID: 'THIRD_TAB_BAR_BUTTON'
                          }
                        }
                      }
                    }]
                  },
                component: {
                  id: "Broker",
                  name: "app1"
                }
              }]
            }
          }
        },
             
      },
    });
  });